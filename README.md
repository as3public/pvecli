# Proxmox Virtual Environment CLI 

## Automatic installation
Install PVE command-line by running the following commands in your unix-like shell (e.g. bash):
```
bash <(curl -s https://gitlab.com/as3public/pvecli/-/raw/main/install.sh)
```

> PVE is a tool to help users to manage Proxmox cluster using command-line fashion without using the web console.

