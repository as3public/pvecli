#!/usr/bin/env bash

set -e

RESET='\033[0m'
RED='\033[38;5;1m'
GREEN='\033[38;5;2m'
YELLOW='\033[38;5;3m'
MAGENTA='\033[38;5;13m'
CYAN='\033[38;5;6m'
WHITE='\033[38;5;15m'

###################################### Configuration code ######################################
LOG_COLOR=yes
PVECLI_BASE_DIR=${PVECLI_BASE_DIR:-$HOME/.pvecli}

########################################## Functions ###########################################
task_message() { printf "%b\n" "${RESET}[ ${CYAN}$1${RESET} ]"; }
check_passed_message() { printf "%b - %s\n" "${RESET}[ ${GREEN}OK${RESET} ]" "$1"; }
check_warned_message() { printf "%b - %s\n" "${RESET}[ ${YELLOW}WARN${RESET} ]" "$1"; }
check_failed_message() { printf "%b %s\n" "${RESET}[${RED}FAILED${RESET}]" "$1"; }
add_recommendation() { printf "%b %s\n" "${RESET}[${YELLOW}$1${RESET}]" "$2"; }

################################## Pre-check validation code ###################################
pre_check_success=true
messages=()
recommendation_messages=()
platform=$(uname | tr '[:upper:]' '[:lower:]')

if [[ "$(id -u)" != "0" ]]; then
  messages+=("$(check_failed_message 'ERR1001: Root user NOT detected. This script requires root privileged to execute.')")
  recommendation_messages+=("$(add_recommendation 'ERR1001' 'RECOMMENDATION: Run `whoami` or `id -u` to confirm you are root user and returning UID=0')")
  pre_check_success=false
else
  messages+=("$(check_passed_message "Running as user `whoami`")")
fi

if ! command -v git >/dev/null; then
  messages+=("$(check_failed_message 'ERR1005: git command is missing!')")
  recommendation_messages+=("$(add_recommendation 'ERR1005' 'RECOMMENDATION: Git CLI is required.  Please install before continue')")
  pre_check_success=false
else
  messages+=("$(check_passed_message 'Found `git` command')")
fi

################################## BEGIN Installation ###################################

echo;echo "#################### BEGIN Pre-Installation Checks ###############################"
echo
for msg in "${messages[@]}"; do
  echo $msg
done
echo
for msg in "${recommendation_messages[@]}"; do
  echo $msg
done
[[ "$pre_check_success" = false ]] && exit 1

echo;echo "#################### BEGIN Installation ##########################################"
echo
task_message "1. Installing pvecli command-line tool"
rm -rf "$PVECLI_BASE_DIR"  &> /dev/null || :

git_url_prefix="https://gitlab.com"
git clone "${git_url_prefix}/as3public/pvecli.git" "$PVECLI_BASE_DIR"
chmod +x $PVECLI_BASE_DIR/bin/pvecli
ln -sf $PVECLI_BASE_DIR/bin/pvecli /usr/local/bin/pvecli
