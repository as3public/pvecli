#!/usr/bin/env sh

# Bash color code formatting:  https://misc.flogisoft.com/bash/tip_colors_and_formatting
# Constants
# Constants
RESET=''
RED=''
GREEN=''
YELLOW=''
MAGENTA=''
CYAN=''
WHITE=''
if [ "${LOG_COLOR:-no}" = "yes" ]; then
  RESET='\033[0m'
  RED='\033[38;5;1m'
  GREEN='\033[38;5;2m'
  YELLOW='\033[38;5;3m'
  MAGENTA='\033[38;5;13m'
  CYAN='\033[38;5;6m'
  WHITE='\033[38;5;15m'
fi 

########################
# Stdout_print
#########################
stdout_print() {
  printf "%b\\n" "${*}" >&2
}

########################
# Stdout print
#########################
log_format() {
  local message="$(shift;echo "$@")"
  stdout_print "${WHITE}$(date +%F' '%T) ${1} ${CYAN}${LOG_MODULE:+$LOG_MODULE }${RESET}- ${message}"
}

########################
# Display information output
########################
log_info() {
  log_format "${GREEN}INFO ${RESET}" "${*}"
}

########################
# Display warning output
########################
log_warn() {
  log_format "${YELLOW}WARN ${RESET}" "${*}"
}

########################
# Display error output
########################
log_error() {
  log_format "${RED}ERROR${RESET}" "${*}"
}

########################
# Only display output when 
# LOG_DEBUG is set to yes
########################
log_debug() {
  if [ "${LOG_DEBUG:-no}" = "yes" ]; then
    log_format "${MAGENTA}DEBUG${RESET}"  "${*}"
  fi
}
